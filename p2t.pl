#!/usr/bin/perl
#
# Copyright (C) 2002 Red Hat, Inc.
#
# Convertion tool from zipcode.t to zipcode.p
#

while(<>) {
  ($zip, $kanji, $speech, $freq)  = split(' ');
  if( $speech ne "#CN" ) {
    print STDERR "This file is broken.\n";
    exit;
  }
  print "$zip $speech $kanji\n";
}
