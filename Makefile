DISTFILES= Makefile COPYING README.jp bracecut.pl jigyosyo.t \
	jigyosyo2canna.pl ken_all2canna.pl p2t.pl zen2han.c zipcode.t

DISTNAME=zipcode-`date '+%Y%m%d'`

all: zipcode.t jigyosyo.t

zipcode.p: zipcode.raw zen2han ken_all2canna.pl bracecut.pl
	./zen2han zipcode.raw | perl ken_all2canna.pl | \
		 perl bracecut.pl > zipcode.p
	@echo "zipcode.p:"
	LANG=ja_JP.eucJP wc zipcode.p

jigyosyo.t: jigyosyo.raw zen2han bracecut.pl
	./zen2han jigyosyo.raw | perl jigyosyo2canna.pl > jigyosyo.t

zipcode.t: zipcode.p p2t.pl
	cat zipcode.p | sort | perl p2t.pl > zipcode.t

zipcode.raw: ken_all.csv
	nkf -S -e ken_all.csv > zipcode.raw

jigyosyo.raw: jigyosyo.csv
	nkf -S -e jigyosyo.csv > jigyosyo.raw

download:
	rm -rf ken_all.lzh jigyosyo.lzh ken_all.csv jigyosyo.csv
	wget http://www.post.japanpost.jp/zipcode/dl/kogaki/lzh/ken_all.lzh
	wget http://www.post.japanpost.jp/zipcode/dl/jigyosyo/lzh/jigyosyo.lzh
	lha e ken_all.lzh
	lha e jigyosyo.lzh
	touch *.csv

zen2han: zen2han.c
	gcc -o zen2han zen2han.c

dist:
	mkdir -p $(DISTNAME)
	cp $(DISTFILES) $(DISTNAME)
	tar cjvf $(DISTNAME).tar.bz2 $(DISTNAME)
	rm -rf $(DISTNAME)
